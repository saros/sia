<h1>Sia - Knowledge Base Application</h1>

This repository contains Sia - a Knowledge Base Application which offers integration for the Gerrit Code Review System.

It currently consists of two projects:
* The Web Application (SiaWebApplication) which gives easy access to the underlying database, does all the logical work and offers a REST API.
* The Gerrit Plugin (SiaGerritPlugin) which integrates the Knowledge Base into the Gerrit Code Review System.

<h2>Installation</h2>

<h3>SQL DB</h3>

The Web application needs an SQL database and a service user account with sufficient privileges to store the Knowledge Base entries.
To do that, log into a mysql shell with root access and enter:

* CREATE DATABASE dbSia;
* GRANT ALL PRIVILEGES ON dbSia.\* To 'siaDbUser'@'localhost' IDENTIFIED BY 'siaInitialPW';

This creates a database and a user with all rights to that database. The password should be changed (and then updated in /SiaWebApplication/WebContent/META-INF/context.xml).

To create the tables and stored procedures needed, open a shell, navigate to /SiaWebApplication and enter:

* mysql -u siaDbUser -p < db\_initialization.sql

.. where you will need to enter the password for the service user.

<h3>Web Application</h3>

To install the web application, navigate to /SiaWebApplication, open a shell and enter:

* ant package -Dtomcat.home=*%local tomcat directory%*

to create the .war file in SiaWebApplication/ or

* ant deploy -Dtomcat.home=*%local tomcat directory%*

to deploy the .war file directly to the tomcat webapps directory.

After that, rename */SiaWebApplication/EXAMPLE-SiaWebApplication.xml* to *SiaWebApplication.xml*, update the credentials and path to the database (first two entries) and place it in

* *%local tomcat directory%*/conf/*%engine name%*/*%host name%*/

* for example: /opt/tomcat/conf/CATALINA/localhost/SiaWebApplication.xml

<h3>Gerrit Plugin</h3>

To install the Gerrit plugin, you must change the value of the variable 'siaWebUrlHost' in /SiaGerritPlugin/src/main/java/de/fu\_berlin/inf/sia/RestApiServlet.java to the url where you host the web application. Then you can navigate to /SiaGerritPlugin, open a shell and enter:

* mvn package

Then navigate to /SiaGerritPlugin/target/ and copy the .war file into the 'plugins' folder of your Gerrit server. You may need to restart it.

<h2>Usage</h2>

When writing an inline draft comment, write

* 'WikiIndex':GET
* (for example: MustReleaseColors:GET)

at the start of a comment to insert the description of the corresponding entry in the Sia Knowledge Base.

Likewise, write

* 'WikiIndex':POST
* (for example: MustReleaseColors:POST)

at the start of a comment to submit your comment as a draft.

Note that the WikiIndex must start with a capital letter and must only contain letters.

The plugin will update your draft comment as soon as you hit 'Save'.

<h2>Known limitations</h2>

* Only inline comments are supported
* Plugin is only active when viewing a single file, not when comparing two (url must look like '/c/%change\_no%/%patch\_no%/%file\_name%.%file\_extension%', for example '/c/132/2/test.txt')
* Plugin may not work as expected when there are multiple comments in one line (as comments are currently identified by their line + file)
