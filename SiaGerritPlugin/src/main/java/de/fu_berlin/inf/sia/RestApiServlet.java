package de.fu_berlin.inf.sia;

import com.google.inject.Singleton;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Singleton
class RestApiServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static String siaWebUrlHost = "http://localhost:8081/";
    private static final String TEXT_PLAIN = "text/plain";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
        throws IOException {
        String pathInfo = req.getPathInfo();

        Pattern postPattern = Pattern.compile("^\\/postEntry\\/$");
        Matcher postMatcher = postPattern.matcher(pathInfo);

        if (postMatcher.find()) {

            BufferedReader reader = req.getReader();

            String jsonPostRequest = reader.readLine();

            String url = siaWebUrlHost + "SiaWebApplication/DraftAdd?apicall=true";

            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);

            StringEntity entity = new StringEntity(jsonPostRequest);

            httpPost.setEntity(entity);
            CloseableHttpResponse response = httpclient.execute(httpPost);
            try {

                if (response.getStatusLine().getStatusCode() == 200) {
                    res.setContentType(TEXT_PLAIN);
                    res.getWriter().write("Draft added.");
                } else {
                    res.setContentType(TEXT_PLAIN);
                    res.getWriter().write("Draft was not added.");
                }
            } finally {
                response.close();
                httpclient.close();
            }
        } else {
            res.setContentType(TEXT_PLAIN);
            res.getWriter().write("no post match");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
        throws IOException {

        String pathInfo = req.getPathInfo();

        Pattern getPattern = Pattern.compile("^\\/getEntry\\/(\\w+)$");
        Matcher getMatcher = getPattern.matcher(pathInfo);

        if (getMatcher.find()) {
            String wikiIndex = getMatcher.group(1);
            String url = siaWebUrlHost + "SiaWebApplication/EntryDetail?id="
                + wikiIndex + "&apicall=true";

            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpGet httpget = new HttpGet(url);
            CloseableHttpResponse response = httpclient.execute(httpget);
            try {
                if (response.getStatusLine().getStatusCode() == 200) {
                    HttpEntity httpEntity = response.getEntity();
                    String content = EntityUtils.toString(httpEntity);
                    res.setContentType("application/json");
                    PrintWriter out = res.getWriter();
                    out.println(content);
                } else {
                    res.setContentType(TEXT_PLAIN);
                    res.getWriter().write("Entry not found.");
                }
            } finally {
                response.close();
                httpclient.close();
            }

        } else {
            res.setContentType(TEXT_PLAIN);
            res.getWriter().write("no get match");
        }

    }
}
