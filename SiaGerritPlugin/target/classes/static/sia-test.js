// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

Gerrit.install(function(self) {
	Gerrit.on("history", function(url) {
		
		var regexPattern = /^\/c\/(\d*)\/(\d*)\/([a-z]*)\.([a-z]*)$/i;
		
		if (regexPattern.test(url)) {
			var matches = url.match(regexPattern);
			var changeID = matches[1];
			var patchID = matches[2];
			var fileName = matches[3];
			var fileExtension = matches[4];
					
			var target = document.getElementById("gerrit_body");
			
			var config = {
				attributes: false,
				childList: true,
				characterData: false,
				subtree: true
			};
			
			var observer = new MutationObserver(function(mutations) {
				
				var draftBoxButtons = document.getElementsByClassName("com-google-gerrit-client-diff-DraftBox_BinderImpl_GenCss_style-button");
				var newSiaButtonDict = {};
				for (i = 0; i < draftBoxButtons.length; i++) {
				    if (draftBoxButtons[i].children[0].innerHTML == "Save") {
				    	var lineNr = draftBoxButtons[i].parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.children[0].children[0].innerHTML;
				    	newSiaButtonDict[lineNr] = draftBoxButtons[i];
				    }
				}				

				if (window.oldSiaButtonDict === undefined) {
				    window.oldSiaButtonDict = {};
				}
				
				for(key in newSiaButtonDict){
					if (!(key in window.oldSiaButtonDict)) {
						window.oldSiaButtonDict[key] = newSiaButtonDict[key];
						newSiaButtonDict[key].addEventListener('click', function() {
				
							var clickedButton = this;
					    	var lineNr = clickedButton.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.children[0].children[0].innerHTML;
							var draftsUrl = "/changes/" + changeID + "/revisions/" + patchID + "/drafts";
				
							Gerrit.get(draftsUrl, function(filePaths) {
								for (filePath in filePaths) {
									for (index in filePaths[filePath]) {
										var commentLine = filePaths[filePath][index]["line"];
										if (lineNr == commentLine) {
											var commentID = filePaths[filePath][index]["id"];
											var commentMessage = filePaths[filePath][index]["message"];
											
											var regexPatternGet = /^([A-Z][a-z]+(?:[A-Z][a-z]+)+):GET/;
											var regexPatternPost = /^([A-Z][a-z]+(?:[A-Z][a-z]+)+):POST((?:.|\n)*)$/;
											
											if (regexPatternGet.test(commentMessage)) {
												
												var matches = commentMessage.match(regexPatternGet);
												var siaEntryId = matches[1];
												
												window.siaCommentMessage = commentMessage;
												window.siaCommentUrl = "/changes/" + changeID + "/revisions/" + patchID + "/drafts/" + commentID;
												
												var siaGetUrl = "api/getEntry/" + siaEntryId;
												
												self.get(siaGetUrl, function (entryObject) {
													
													if (entryObject == "Entry not found.") {
														var	newCommentMessage = window.siaCommentMessage + "\n\n(Sia was unable to find a corresponding entry on " + new Date() + ".)";
													}
													else {
														var entryId = entryObject["id"];
														var entryDescription = entryObject["description"];

														var regexPattern = /^([A-Z][a-z]+(?:[A-Z][a-z]+)+):GET/;
														var replacementString = entryId + ": \n" + entryDescription + "\n";
														
														var newCommentMessage = window.siaCommentMessage.replace(regexPattern, replacementString);
														newCommentMessage += "\n\n(This draft was altered by Sia on " + new Date() + ".)";
													}
													Gerrit.put(
															window.siaCommentUrl,
															{message: newCommentMessage},
															function (r) {
																window.location.reload(false);
															}
													);
												});
												
											}
											else if (regexPatternPost.test(commentMessage)) {

												var matches = commentMessage.match(regexPatternPost);
												var newEntryId = matches[1];
												var newDescription = matches[2];
												var draftAuthor = self.getCurrentUser()["name"];
												var siaPostUrl = "api/postEntry/"
												
												window.siaCommentMessage = commentMessage;
												window.siaCommentUrl = "/changes/" + changeID + "/revisions/" + patchID + "/drafts/" + commentID;
												
												self.post(siaPostUrl, {"id": newEntryId, "description": newDescription, "author": draftAuthor}, function(r) {
													
													
													if (r == "Draft added.") {
														
														var regexPattern = /^([A-Z][a-z]+(?:[A-Z][a-z]+)+):POST/;
														var replacementString = newEntryId + ": \n" + newDescription + "\n";
														
														var newCommentMessage = window.siaCommentMessage.replace(regexPattern, replacementString);
														newCommentMessage += "\n\n(This draft was successfully sent to Sia on " + new Date() + ".)";
														
														Gerrit.put(
																window.siaCommentUrl,
																{message: newCommentMessage},
																function (r) {
																	window.location.reload(false);
																}
														);
													}
													else {
														var newCommentMessage = window.siaCommentMessage + "\n\n(Sia was unable to add this draft on " + new Date() + ".)";
														
														Gerrit.put(
																window.siaCommentUrl,
																{message: newCommentMessage},
																function (r) {
																	window.location.reload(false);
																}
														);
													}
												});
											}
										}
									}
								}
							});
						}, false);
					}
				}
				window.oldSiaButtonDict = newSiaButtonDict;
			});
			observer.observe(target, config);
		}
	});
});
