USE dbSia;

DROP TABLE IF EXISTS draft;
DROP TABLE IF EXISTS entry;

CREATE TABLE `entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wiki_index` varchar(64) CHARACTER SET utf8 NOT NULL,
  `version` int(11) NOT NULL,
  `description` mediumtext NOT NULL,
  `author` varchar(256) CHARACTER SET utf8 NOT NULL,
  `reviewer` varchar(256) CHARACTER SET utf8 NOT NULL,
  `is_newest` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_edited` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `version_constraint` (`wiki_index`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `draft` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `description` mediumtext NOT NULL,
  `author` varchar(256) CHARACTER SET utf8 NOT NULL,
  `is_pending` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_edited` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `version_constraint` (`entry_id`,`version`),
  KEY `fk_entry_idx` (`entry_id`),
  CONSTRAINT `fk_entry` FOREIGN KEY (`entry_id`) REFERENCES `entry` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP PROCEDURE IF EXISTS add_draft;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `add_draft`(param_wiki_index NVARCHAR(64), param_description NVARCHAR(20000), param_author NVARCHAR(256))
BEGIN

	DECLARE var_entry_id INT DEFAULT 0;
    DECLARE var_draft_version INT DEFAULT 0;

	IF NOT EXISTS (
	
		SELECT *
        FROM entry
        WHERE wiki_index = param_wiki_index
	
    ) THEN
    
		INSERT INTO entry (wiki_index, version, description, author, reviewer, is_newest)
		VALUES (param_wiki_index, 0, "First draft pending...", "n.a.", "n.a.", 1);
        SET var_draft_version = 1;
        
	ELSE
		
        SELECT MAX(d.version) + 1 INTO var_draft_version
        FROM draft d
        LEFT JOIN entry e ON d.entry_id = e.id
        WHERE e.wiki_index = param_wiki_index;
    
    END IF;
        
	SELECT id INTO var_entry_id
	FROM entry
	WHERE wiki_index = param_wiki_index AND is_newest = 1;
	
	INSERT INTO draft (entry_id, version, description, author, is_pending)
	VALUES (var_entry_id, var_draft_version, param_description, param_author, 1);
        
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS get_draft_by_ID;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `get_draft_by_ID`(param_id INT)
BEGIN
	SELECT * FROM draft WHERE id = param_id;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS get_entry_by_ID;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `get_entry_by_ID`(param_id INT)
BEGIN
	SELECT * FROM entry WHERE id = param_id;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS get_newest_entry_by_draft_id;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `get_newest_entry_by_draft_id`(param_draft_id INT)
BEGIN

	DECLARE var_entry_id INT DEFAULT "0";
	DECLARE var_wiki_index NVARCHAR(64) DEFAULT "default_wiki_index";
    
    SELECT entry_id INTO var_entry_id
    FROM draft
    WHERE id = param_draft_id;
    
	SELECT wiki_index INTO var_wiki_index
    FROM entry
    WHERE id = var_entry_id;

	SELECT *
    FROM entry
    WHERE wiki_index = var_wiki_index AND is_newest = 1;
    
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS get_newest_entry_by_wiki_index;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `get_newest_entry_by_wiki_index`(param_wiki_index NVARCHAR(64))
BEGIN
	SELECT *
    FROM entry
    WHERE wiki_index = param_wiki_index AND is_newest = 1;
END$$
DELIMITER ;



DROP PROCEDURE IF EXISTS merge_draft_into_new_entry;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `merge_draft_into_new_entry`(param_draft_id INT, param_entry_wiki_index NVARCHAR(64), param_new_description NVARCHAR(20000), param_new_reviewer NVARCHAR(256))
BEGIN
	DECLARE var_version INT DEFAULT 0;
    DECLARE var_author NVARCHAR(256) DEFAULT "default_author";

	SELECT version INTO var_version
	FROM entry
	WHERE wiki_index = param_entry_wiki_index AND is_newest = 1;
    
    SELECT author INTO var_author
    FROM draft
    WHERE id = param_draft_id;
    
    UPDATE entry
    SET is_newest = 0
    WHERE wiki_index = param_entry_wiki_index AND is_newest = 1;
    
    UPDATE draft
    SET is_pending = 0
    WHERE id = param_draft_id;

	INSERT INTO entry (wiki_index, version, description, author, reviewer, is_newest)
    VALUES (
    param_entry_wiki_index,
    (var_version + 1),
    param_new_description,
    var_author,
    param_new_reviewer,
    1
    );
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS reject_draft_by_id;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `reject_draft_by_id`(param_draft_id INT)
BEGIN
	UPDATE draft
    SET is_pending = 0
    WHERE id = param_draft_id;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS select_all_current_entries;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `select_all_current_entries`()
BEGIN
	SELECT * FROM entry WHERE is_newest=1;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS select_all_drafts;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `select_all_drafts`()
BEGIN
	SELECT * FROM draft;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS select_all_entries;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `select_all_entries`()
BEGIN
	SELECT * FROM entry;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS select_all_pending_drafts;
DELIMITER $$
CREATE DEFINER=`siaDbUser`@`localhost` PROCEDURE `select_all_pending_drafts`()
BEGIN
	SELECT * FROM draft WHERE is_pending=1;
END$$
DELIMITER ;
