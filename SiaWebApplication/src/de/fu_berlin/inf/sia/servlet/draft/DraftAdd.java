package de.fu_berlin.inf.sia.servlet.draft;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import de.fu_berlin.inf.sia.dao.DraftDAO;

/**
 * Servlet implementation class DraftAdd
 */
@WebServlet("/DraftAdd")
public class DraftAdd extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Resource(name = "jdbc/kb")
    private static transient DataSource dataSource;
    private static DraftDAO draftDAO;

    @Override
    public synchronized void init() {
        draftDAO = new DraftDAO(dataSource);
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DraftAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/draftAdd.jsp").forward(request,
            response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        try {
            if ("true".equals(request.getParameter("apicall"))) {
                BufferedReader reader = request.getReader();
                String jsonPostRequest = reader.readLine();
                
                JsonElement jElement = new JsonParser().parse(jsonPostRequest);
                JsonObject jObject = jElement.getAsJsonObject();
                JsonElement jWikiIndexElement = jObject.get("id");
                String jWikiIndex = jWikiIndexElement.getAsString();
                JsonElement jDescriptionElement = jObject.get("description");
                String jDescription = jDescriptionElement.getAsString();
                JsonElement jAuthorElement = jObject.get("author");
                String jAuthor = jAuthorElement.getAsString();
                
                draftDAO.add(jWikiIndex, jDescription, jAuthor);
                response.setContentType("text/plain");
                response.getWriter().write("Draft added.");
            } else {
                String wikiIndex = request.getParameter("txtWikiIndex");
                String description = request.getParameter("txtDescription");
                String author = request.getParameter("txtAuthor");
                draftDAO.add(wikiIndex, description, author);
                response.sendRedirect(request.getContextPath() + "/DraftOverview");
            }
        } catch (SQLException e) {
            throw new ServletException("Cannot save new draft to DB", e);
        }
    }
}
