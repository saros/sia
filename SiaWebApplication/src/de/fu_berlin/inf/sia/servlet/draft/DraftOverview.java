package de.fu_berlin.inf.sia.servlet.draft;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import de.fu_berlin.inf.sia.dao.DraftDAO;
import de.fu_berlin.inf.sia.entity_beans.Draft;

/**
 * Servlet implementation class DraftOverview
 */
@WebServlet("/DraftOverview")
public class DraftOverview extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Resource(name = "jdbc/kb")
    private static transient DataSource dataSource;
    private static DraftDAO draftDAO;

    @Override
    public synchronized void init() {
        draftDAO = new DraftDAO(dataSource);
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DraftOverview() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Draft> drafts = draftDAO.listPending();
            request.setAttribute("drafts", drafts);
            request.getRequestDispatcher("/draftOverview.jsp").forward(request,
                response);
        } catch (SQLException e) {
            throw new ServletException("Cannot obtain drafts from DB", e);
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
