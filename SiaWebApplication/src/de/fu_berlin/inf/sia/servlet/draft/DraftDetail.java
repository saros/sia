package de.fu_berlin.inf.sia.servlet.draft;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import de.fu_berlin.inf.sia.dao.DraftDAO;
import de.fu_berlin.inf.sia.dao.EntryDAO;
import de.fu_berlin.inf.sia.entity_beans.Draft;
import de.fu_berlin.inf.sia.entity_beans.Entry;

/**
 * Servlet implementation class DraftDetail
 */
@WebServlet("/DraftDetail")
public class DraftDetail extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Resource(name = "jdbc/kb")
    private static transient DataSource dataSource;
    private static DraftDAO draftDAO;
    private static EntryDAO entryDAO;

    @Override
    public synchronized void init() {
        draftDAO = new DraftDAO(dataSource);
        entryDAO = new EntryDAO(dataSource);
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DraftDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        try {
            Long id = Long.parseLong(request.getParameter("id"));
            Draft draft = draftDAO.draft(id);
            request.setAttribute("draft", draft);
            Entry entry = entryDAO.newestEntryByDraftId(id);
            request.setAttribute("entry", entry);
            request.getRequestDispatcher("/draftDetail.jsp").forward(request,
                response);
        } catch (SQLException e) {
            throw new ServletException("Cannot obtain drafts from DB", e);
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        try {

            Long id = Long.parseLong(request.getParameter("hdnDraftId"));

            if (request.getParameter("submit") != null) {
                String wikiIndex = request.getParameter("hdnWikiIndex");
                String newDescription = request
                    .getParameter("txtNewDescription");
                String newReviewer = request.getParameter("txtNewReviewer");
                draftDAO.mergeDraftIntoNewEntry(id, wikiIndex,
                    newDescription, newReviewer);
                response
                    .sendRedirect(request.getContextPath() + "/DraftOverview");
            } else {
                draftDAO.reject(id);
                response
                    .sendRedirect(request.getContextPath() + "/DraftOverview");
            }
        } catch (SQLException e) {
            throw new ServletException("Cannot save new entry to DB", e);
        }
    }

}
