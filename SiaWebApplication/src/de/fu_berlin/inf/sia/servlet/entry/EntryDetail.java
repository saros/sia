package de.fu_berlin.inf.sia.servlet.entry;

import de.fu_berlin.inf.sia.dao.EntryDAO;
import de.fu_berlin.inf.sia.entity_beans.Entry;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class EntryDetail
 */
@WebServlet("/EntryDetail")
public class EntryDetail extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Resource(name = "jdbc/kb")
    private static transient DataSource dataSource;
    private static EntryDAO entryDAO;

    @Override
    public synchronized void init() {
        entryDAO = new EntryDAO(dataSource);
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EntryDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
            try {
                if ("true".equals(request.getParameter("apicall"))) {
                    String wikiIndex = request.getParameter("id");
                    Entry entry = entryDAO.newestEntryByWikiIndex(wikiIndex);
                    response.setContentType("application/json");
                    JsonObject jsonObject = Json.createObjectBuilder()
                        .add("id", entry.getWikiIndex())
                        .add("description", entry.getDescription())
                        .build();
                    PrintWriter out = response.getWriter();
                    out.println(jsonObject);
                } else {
                    Long id = Long.parseLong(request.getParameter("id"));
                    Entry entry = entryDAO.entry(id);
                    request.setAttribute("entry", entry);
                    request.getRequestDispatcher("/entryDetail.jsp").forward(request,
                        response);
                }
            } catch (SQLException e) {
                throw new ServletException("Cannot obtain drafts from DB", e);
            }
        }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
