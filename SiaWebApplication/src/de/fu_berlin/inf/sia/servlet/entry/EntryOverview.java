package de.fu_berlin.inf.sia.servlet.entry;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import de.fu_berlin.inf.sia.dao.EntryDAO;
import de.fu_berlin.inf.sia.entity_beans.Entry;

/**
 * Servlet implementation class EntryOverview
 */
@WebServlet("/EntryOverview")
public class EntryOverview extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Resource(name = "jdbc/kb")
    private static transient DataSource dataSource;
    private static EntryDAO entryDAO;

    @Override
    public synchronized void init() {
        entryDAO = new EntryDAO(dataSource);
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EntryOverview() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Entry> entries = entryDAO.listCurrent();
            request.setAttribute("entries", entries);
            request.getRequestDispatcher("/entryOverview.jsp").forward(request,
                response);
        } catch (SQLException e) {
            throw new ServletException("Cannot obtain entries from DB", e);
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
