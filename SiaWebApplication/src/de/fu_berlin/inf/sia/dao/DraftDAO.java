package de.fu_berlin.inf.sia.dao;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import de.fu_berlin.inf.sia.entity_beans.Draft;

public class DraftDAO implements Serializable {

    private static final long serialVersionUID = 1L;
    private transient DataSource dataSource;

    public DraftDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    private static Draft getDraftFromResultSet(ResultSet resultSet) throws SQLException {
        Draft draft = new Draft();
        draft.setId(resultSet.getLong("id"));
        draft.setEntryId(resultSet.getLong("entry_id"));
        draft.setVersion(resultSet.getLong("version"));
        draft.setDescription(resultSet.getString("description"));
        draft.setAuthor(resultSet.getString("author"));
        draft.setIsPending(resultSet.getBoolean("is_pending"));
        draft.setCreated(resultSet.getTimestamp("created"));
        draft.setLastEdited(resultSet.getTimestamp("last_edited"));
        return draft;
    }

    public List<Draft> list() throws SQLException {
        List<Draft> drafts = new ArrayList<>();
        try (
            Connection connection = dataSource.getConnection();
            CallableStatement statement = connection
                .prepareCall("{call select_all_drafts()}");
            ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                drafts.add(getDraftFromResultSet(resultSet));
            }
        }
        return drafts;
    }

    public List<Draft> listPending() throws SQLException {
        List<Draft> drafts = new ArrayList<>();
        try (
            Connection connection = dataSource.getConnection();
            CallableStatement statement = connection
                .prepareCall("{call select_all_pending_drafts()}");
            ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                drafts.add(getDraftFromResultSet(resultSet));
            }
        }
        return drafts;
    }

    public Draft draft(Long id) throws SQLException {
        Connection connection = null;
        CallableStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection
                .prepareCall("{call get_draft_by_ID(?)}");
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            resultSet.next();
            return getDraftFromResultSet(resultSet);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    public void mergeDraftIntoNewEntry(Long draftId, String wikiIndex,
        String newDescription, String newReviewer) throws SQLException {
        try (
            Connection connection = dataSource.getConnection();
            CallableStatement statement = connection
                .prepareCall("{call merge_draft_into_new_entry(?, ?, ?, ?)}");
        ) {
            statement.setLong(1, draftId);
            statement.setString(2, wikiIndex);
            statement.setString(3, newDescription);
            statement.setString(4, newReviewer);
            statement.executeQuery();
        }
    }

    public void add(String wikiIndex, String description, String author)
        throws SQLException {
        try (
            Connection connection = dataSource.getConnection();
            CallableStatement statement = connection
                .prepareCall("{call add_draft(?, ?, ?)}");
        ) {
            statement.setString(1, wikiIndex);
            statement.setString(2, description);
            statement.setString(3, author);
            statement.executeQuery();
        }
    }

    public void reject(Long id) throws SQLException {
        try (
            Connection connection = dataSource.getConnection();
            CallableStatement statement = connection
                .prepareCall("{call reject_draft_by_id(?)}");
        ) {
            statement.setLong(1, id);
            statement.executeQuery();
        }
    }
}
