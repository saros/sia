package de.fu_berlin.inf.sia.dao;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import de.fu_berlin.inf.sia.entity_beans.Entry;

public class EntryDAO implements Serializable {

    private static final long serialVersionUID = 1L;
    private transient DataSource dataSource;

    public EntryDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    private static Entry getEntryFromResultSet(ResultSet resultSet) throws SQLException {
        Entry entry = new Entry();
        entry.setId(resultSet.getLong("id"));
        entry.setWikiIndex(resultSet.getString("wiki_index"));
        entry.setVersion(resultSet.getLong("version"));
        entry.setDescription(resultSet.getString("description"));
        entry.setAuthor(resultSet.getString("author"));
        entry.setReviewer(resultSet.getString("reviewer"));
        entry.setIsNewest(resultSet.getBoolean("is_newest"));
        entry.setCreated(resultSet.getTimestamp("created"));
        entry.setLastEdited(resultSet.getTimestamp("last_edited"));
        return entry;
    }

    public List<Entry> list() throws SQLException {
        List<Entry> entries = new ArrayList<>();

        try (
            Connection connection = dataSource.getConnection();
            CallableStatement statement = connection
                .prepareCall("{call select_all_entries()}");
            ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                entries.add(getEntryFromResultSet(resultSet));
            }
        }
        return entries;
    }

    public List<Entry> listCurrent() throws SQLException {
        List<Entry> entries = new ArrayList<>();

        try (
            Connection connection = dataSource.getConnection();
            CallableStatement statement = connection
                .prepareCall("{call select_all_current_entries()}");
            ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                entries.add(getEntryFromResultSet(resultSet));
            }
        }

        return entries;
    }

    public Entry entry(Long id) throws SQLException {
        Connection connection = null;
        CallableStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection
                .prepareCall("{call get_entry_by_ID(?)}");
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            resultSet.next();
            return getEntryFromResultSet(resultSet);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    public Entry newestEntryByDraftId(Long draftId) throws SQLException {
        Connection connection = null;
        CallableStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection
                .prepareCall("{call get_newest_entry_by_draft_id(?)}");
            statement.setLong(1, draftId);
            resultSet = statement.executeQuery();
            resultSet.next();
            return getEntryFromResultSet(resultSet);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    public Entry newestEntryByWikiIndex(String wikiIndex) throws SQLException {
        Connection connection = null;
        CallableStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection
                .prepareCall("{call get_newest_entry_by_wiki_index(?)}");
            statement.setString(1, wikiIndex);
            resultSet = statement.executeQuery();
            resultSet.next();

            return getEntryFromResultSet(resultSet);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

}
