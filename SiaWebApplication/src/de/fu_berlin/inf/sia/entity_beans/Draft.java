/**
 * 
 */
package de.fu_berlin.inf.sia.entity_beans;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author dennis
 *
 */
public class Draft implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long entryId;
    private Long version;
    private String description;
    private String author;
    private boolean isPending;
    private Timestamp created;
    private Timestamp lastEdited;

    /**
     * 
     */
    public Draft() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param id
     * @param entryId
     * @param version
     * @param description
     * @param author
     * @param isPending
     * @param created
     * @param lastEdited
     */
    public Draft(Long id, Long entryId, Long version, String description,
        String author, boolean isPending, Timestamp created,
        Timestamp lastEdited) {
        super();
        this.id = id;
        this.entryId = entryId;
        this.version = version;
        this.description = description;
        this.author = author;
        this.isPending = isPending;
        this.created = created;
        this.lastEdited = lastEdited;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the entry_id
     */
    public Long getEntryId() {
        return entryId;
    }

    /**
     * @param entryId
     *            the entry_id to set
     */
    public void setEntryId(Long entryId) {
        this.entryId = entryId;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author
     *            the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the is_pending
     */
    public boolean isIsPending() {
        return isPending;
    }

    /**
     * @param isPending
     *            the is_pending to set
     */
    public void setIsPending(boolean isPending) {
        this.isPending = isPending;
    }

    /**
     * @return the created
     */
    public Timestamp getCreated() {
        return created;
    }

    /**
     * @param created
     *            the created to set
     */
    public void setCreated(Timestamp created) {
        this.created = created;
    }

    /**
     * @return the last_edited
     */
    public Timestamp getLastEdited() {
        return lastEdited;
    }

    /**
     * @param lastEdited
     *            the last_edited to set
     */
    public void setLastEdited(Timestamp lastEdited) {
        this.lastEdited = lastEdited;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((author == null) ? 0 : author.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result
            + ((description == null) ? 0 : description.hashCode());
        result = prime * result
            + ((entryId == null) ? 0 : entryId.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (isPending ? 1231 : 1237);
        result = prime * result
            + ((lastEdited == null) ? 0 : lastEdited.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Draft other = (Draft) obj;
        if (author == null) {
            if (other.author != null)
                return false;
        } else if (!author.equals(other.author))
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (entryId == null) {
            if (other.entryId != null)
                return false;
        } else if (!entryId.equals(other.entryId))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (isPending != other.isPending)
            return false;
        if (lastEdited == null) {
            if (other.lastEdited != null)
                return false;
        } else if (!lastEdited.equals(other.lastEdited))
            return false;
        if (version == null) {
            if (other.version != null)
                return false;
        } else if (!version.equals(other.version))
            return false;
        return true;
    }

}
