/**
 * 
 */
package de.fu_berlin.inf.sia.entity_beans;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author dennis
 *
 */
public class Entry implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String wikiIndex;
    private Long version;
    private String description;
    private String author;
    private String reviewer;
    private boolean isNewest;
    private Timestamp created;
    private Timestamp lastEdited;

    /**
     * 
     */
    public Entry() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param id
     * @param wikiIndex
     * @param version
     * @param description
     * @param author
     * @param reviewer
     * @param isNewest
     * @param created
     * @param lastEdited
     */
    public Entry(Long id, String wikiIndex, Long version, String description,
        String author, String reviewer, boolean isNewest, Timestamp created,
        Timestamp lastEdited) {
        super();
        this.id = id;
        this.wikiIndex = wikiIndex;
        this.version = version;
        this.description = description;
        this.author = author;
        this.reviewer = reviewer;
        this.isNewest = isNewest;
        this.created = created;
        this.lastEdited = lastEdited;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the wiki_index
     */
    public String getWikiIndex() {
        return wikiIndex;
    }

    /**
     * @param wikiIndex
     *            the wiki_index to set
     */
    public void setWikiIndex(String wikiIndex) {
        this.wikiIndex = wikiIndex;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author
     *            the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the author
     */
    public String getReviewer() {
        return reviewer;
    }

    /**
     * @param author
     *            the author to set
     */
    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    /**
     * @return the is_newest
     */
    public boolean isIsNewest() {
        return isNewest;
    }

    /**
     * @param isNewest
     *            the is_newest to set
     */
    public void setIsNewest(boolean isNewest) {
        this.isNewest = isNewest;
    }

    /**
     * @return the created
     */
    public Timestamp getCreated() {
        return created;
    }

    /**
     * @param created
     *            the created to set
     */
    public void setCreated(Timestamp created) {
        this.created = created;
    }

    /**
     * @return the last_edited
     */
    public Timestamp getLastEdited() {
        return lastEdited;
    }

    /**
     * @param lastEdited
     *            the last_edited to set
     */
    public void setLastEdited(Timestamp lastEdited) {
        this.lastEdited = lastEdited;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((author == null) ? 0 : author.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result
            + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (isNewest ? 1231 : 1237);
        result = prime * result
            + ((lastEdited == null) ? 0 : lastEdited.hashCode());
        result = prime * result
            + ((reviewer == null) ? 0 : reviewer.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        result = prime * result
            + ((wikiIndex == null) ? 0 : wikiIndex.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Entry other = (Entry) obj;
        if (author == null) {
            if (other.author != null)
                return false;
        } else if (!author.equals(other.author))
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (isNewest != other.isNewest)
            return false;
        if (lastEdited == null) {
            if (other.lastEdited != null)
                return false;
        } else if (!lastEdited.equals(other.lastEdited))
            return false;
        if (reviewer == null) {
            if (other.reviewer != null)
                return false;
        } else if (!reviewer.equals(other.reviewer))
            return false;
        if (version == null) {
            if (other.version != null)
                return false;
        } else if (!version.equals(other.version))
            return false;
        if (wikiIndex == null) {
            if (other.wikiIndex != null)
                return false;
        } else if (!wikiIndex.equals(other.wikiIndex))
            return false;
        return true;
    }

}
