<%@page import="java.util.Date"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="custom.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script type="text/javascript">
		
			$(document).ready(function() {
			    $('.clickable-row').click(function () {
			        window.document.location = $(this).data("href");
			    });
			});
		</script>

	<title>Draft overview</title>
    <base href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/">
</head>
<body>
	<div class="container">
		<h2>Draft overview (pending)</h2>
		<hr>
		<a href="MainServlet">Main page</a>
		<hr>
		<table class="table">
			<thead>
				<tr>
					<th>Description</th>
					<th>Author</th>
					<th>Version</th>
					<th>Created</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${drafts}" var="draft">
					<tr class="clickable-row"
						data-href="DraftDetail?id=${draft.id}">
						<td><c:out value="${draft.description}" /></td>
						<td><c:out value="${draft.author}" /></td>
						<td>${draft.version}</td>
						<td><fmt:formatDate value="${draft.created}"
								pattern="dd.MM.yyyy HH:mm" /></td>
						<td></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>