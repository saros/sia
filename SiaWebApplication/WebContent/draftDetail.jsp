<%@page import="java.util.Date"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="custom.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Draft detail</title>
    <base href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/">
</head>
<body>
	<div class="container">
		<h2>Draft detail</h2>
		<hr>
		<a href="MainServlet">Main page</a>
		<hr>
		<p>
			<label>ID: </label> ${entry.wikiIndex}
		</p>
		<hr>
		<h3>Current entry</h3>
		<p>
			<label>Entry version: </label> ${entry.version}
		</p>
		<p>
			<label>Author:</label> ${entry.author}
		<p>
		<p>
			<label>Reviewer:</label> ${entry.reviewer}
		</p>
		<p>
			<label>Description:</label>
		</p>
		<p>${entry.description}</p>
		<hr>
		<h3>Draft</h3>
		<p>
			<label>Draft version: </label> ${draft.version}
		</p>
		<p>
			<label>Author:</label> ${draft.author}
		<p>
		<p>
			<label>Description:</label>
		</p>
		<p>${draft.description}</p>
		<hr>
		<c:choose>
			<c:when test="${draft.isPending}">
				<h3>New entry:</h3>
				<form action="${pageContext.request.contextPath}/DraftDetail"
					method="post">
					<input type="hidden" name="hdnDraftId" value="${draft.id}" /> <input
						type="hidden" name="hdnWikiIndex" value="${entry.wikiIndex}" />
					<p>
						<label>Reviewer:</label> <input type="text" name="txtNewReviewer" />
					</p>
					<p>
						<textarea name="txtNewDescription" rows="20" cols="190">${draft.description}</textarea>
					</p>
					<p class="button-row">
						<input type="submit" name="submit" value="submit"></input> <input
							type="submit" name="reject" value="reject"></input>
					</p>
				</form>
			</c:when>
			<c:otherwise>
				<p>
					<label>Draft already merged or rejected.</label>
				</p>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>