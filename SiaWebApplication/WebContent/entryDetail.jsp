<%@page import="java.util.Date"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="custom.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Entry detail</title>
    <base href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/">
</head>
<body>
	<div class="container">
		<c:choose>
			<c:when test="${entry.isNewest}">
				<h2>Entry detail (current)</h2>
			</c:when>
			<c:otherwise>
				<h2>Entry detail (outdated)</h2>
			</c:otherwise>
		</c:choose>
		<hr>
		<a href="MainServlet">Main page</a>
		<hr>
		<p>
			<label>ID: </label> ${entry.wikiIndex}
		</p>
		<p>
			<label>Version: </label> ${entry.version}
		</p>
		<p>
			<label>Author: </label> ${entry.author}
		</p>
		<p>
			<label>Reviewer: </label> ${entry.reviewer}
		</p>
		<p>
			<label>Created: </label>
			<fmt:formatDate value="${entry.created}" pattern="dd.MM.yyyy HH:mm" />
		</p>
		<p>
			<label>Last edited: </label>
			<fmt:formatDate value="${entry.lastEdited}"
				pattern="dd.MM.yyyy HH:mm" />
		</p>
		<p>
			<label>Description:</label>
		</p>
		<p>${entry.description}</p>
	</div>
</body>
</html>