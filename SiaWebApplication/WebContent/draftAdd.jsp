<%@page import="java.util.Date"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="custom.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<title>Add draft</title>
</head>
<body>
	<div class="container">
		<h2>Add draft</h2>
		<hr>
		<a href="MainServlet">Main page</a>
		<hr>
		<form action="${pageContext.request.contextPath}/DraftAdd"
			method="post">
			<p>
				<label>ID (WikiIndex):</label> <input type="text"
					name="txtWikiIndex" />
			</p>
			<p>
				<label>Author:</label> <input type="text" name="txtAuthor" />
			</p>
			<p>
				<textarea name="txtDescription" rows="20" cols="190">${draft.description}</textarea>
			</p>
			<p class="button-row">
				<input type="submit" name="submit" value="submit"></input>
			</p>
		</form>
	</div>
</body>
</html>